const Invitation = require('./Types/Invitation');
const Type = require('./Types/Type')
class QrFactory {

    /**
     *
     * @param type
     * @return Type
     */
    static getType(type)
    {
        switch (type){
            case Invitation.identifier:
                return Invitation;
            default:
                throw new Error(`QR Type not found "${type}"`);
        }
    }

}

module.exports = QrFactory;