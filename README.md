### 🔧 Instalación

Instalar libreria desde repositorio
```bash
npm i https://gitlab.com/migrar-lib-publicas/js/schlabs-qr-js
```
### 🚀 Uso

Importar 'schlabs-qr'
```js
const Qr = require('schlabs-qr');
```


Convertir de objeto js a hash
```js
//Ejemplo

let invitation = {
    user_profile_id: 1,
    user_id: 2,
    timestamp: 3,
    community_id: 4,
    profile_id: null,
    timestamp_qr: 6,
    visitor_profile_id: 7,
    visitor_id: 8,
    schedule: true
}

//Se pueden ocupar constantes de Qr para los tipos
let hash = Qr.arrayToHash(objeto, Invitation.identifier)

//Respuesta: I|1|2|3|4||6|7|8|true
```
Convertir de hash a objeto js
```js
//Ejemplo

let hash = 'I|1|2|3|4||6|7|8|true'

//No es necesario agregar la constante del tipo
let hash = Qr.hashToArray(hash)

//Respuesta:
//{
//    user_profile_id: 1,
//    user_id: 2,
//    timestamp: 3,
//    community_id: 4,
//    profile_id: null,
//    timestamp_qr: 6,
//    visitor_profile_id: 7,
//    visitor_id: 8,
//    schedule: true
//} 
 ```

### 📃 Creación de plantillas

Se debe crear una clase que extienda de la clase `Type` y debe estar ubicada en el directorio *Types* del paquete
```js
...
class NuevoTipo extends Type
...
```
Esta plantilla **debe** tener las siguientes propiedades **ESTATICAS**

**identifier**: 
* Tipo: `string` 
* Descripción: Caracteres de indentificación de plantilla
* Ej: 
```js 
static identifier = 'I' //Identificador de plantilla
```

**properties**: 
* Tipo: `object` 
* Descripción: Objeto con llaves y tipos de plantilla
* Ej:
```js
 static properties = {
    'user_profile_id': 'int',
    'user_id': 'int',
    'timestamp': 'int',
    'community_id': 'int',
    'profile_id': 'int',
    'timestamp_qr': 'int',
    'visitor_profile_id': 'int',
    'visitor_id': 'int',
    'schedule': 'boolean'
}
```
* Tipos soportados: `string`, `int` o `number`, `bool` o `boolean`
