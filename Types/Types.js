const Invitation = require("./Invitation");

class Types {
    static get INVITATION(){
        return (new Invitation).identifier
    }
}

module.exports = Types;
