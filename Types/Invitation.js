const Type = require('./Type')
class Invitation extends Type{
    static identifier = 'I'

    static properties = {
        'user_profile_id': 'int',
        'user_id': 'int',
        'timestamp': 'int',
        'community_id': 'int',
        'profile_id': 'int',
        'timestamp_qr': 'int',
        'visitor_profile_id': 'int',
        'visitor_id': 'int',
        'schedule': 'boolean'
    }
}

module.exports = Invitation;