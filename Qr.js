const QrFactory = require('./QrFactory')
const Invitation = require("./Types/Invitation");
const Types = require("./Types/Types");

class Qr {

    static #SEPARATOR = '|';


    static hashToArray(hash)
    {
        this.validate([
            {name: 'hashToArray.hash', value: hash, type: 'string'},
        ])

        let hashData = hash.split(this.#SEPARATOR);

        let qrType = QrFactory.getType(hashData[0]);

        let typeProperties = qrType.properties;
        let propertyTypes = Object.values(qrType.properties);
        let propertyNames = Object.keys(typeProperties);

        this.validateLength(qrType, hashData.length);

        let newData = {};
        hashData = hashData.slice(1, Object.keys(qrType.properties).length + 1);

        for(let segment in hashData){
            newData[propertyNames[segment]] = this.convertType(hashData[segment], propertyTypes[segment]);
        }

        return newData;
    }

    static arrayToHash(data, type)
    {
        this.validate([
            {name: 'arrayToHash.data', value: data, type: 'object'},
            {name: 'arrayToHash.type', value: type, type: 'string'},
        ])

        let qrType = QrFactory.getType(type);
        this.validateLength(qrType, Object.values(data).length + 1);

        let hash = Object.values(data).join(this.#SEPARATOR)

        return `${qrType.identifier}${this.#SEPARATOR}${hash}`;
    }

    static validateLength(qrType, length)
    {
        if(Object.keys(qrType.properties).length !== length - 1){
            throw new Error('Incorrect number of segments')
        }
    }

    static convertType(data, type)
    {
        if(data === undefined || data === null || data === ''){
            return null
        }

        switch (type){
            case 'string':
                return String(data)
            case 'int':
            case 'number':
                return Number(data)
            case 'boolean':
            case 'bool':
                return data === 'true'
            default:
                return data
        }
    }

    static validate(rules)
    {
        for(let rule in rules){
            if(
                rules[rule].value === null ||
                rules[rule].value === undefined ||
                typeof rules[rule].value !== rules[rule].type
            ){
                throw new Error(
                    `Invalid type for variable "${rules[rule].name}", must be ${rules[rule].type} and it is ${typeof rules[rule].value}`
                )
            }
        }
    }
}

module.exports = Qr;